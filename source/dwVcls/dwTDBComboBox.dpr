﻿library dwTDBComboBox;

uses
     ShareMem,

     //
     dwCtrlBase,

     //
     SynCommons,

     //
     Messages, SysUtils, Variants, Classes, Graphics,
     Controls, Forms, Dialogs, ComCtrls, ExtCtrls,
     DBCtrls,
     StdCtrls, Windows;

//当前控件需要引入的第三方JS/CSS
function dwGetExtra(ACtrl:TComponent):string;stdCall;
begin
     Result    := '[]';
end;

//根据JSON对象AData执行当前控件的事件, 并返回结果字符串
function dwGetEvent(ACtrl:TComponent;AData:String):string;StdCall;
var
    joData  : Variant;
    sValue  : String;
    I       : Integer;
begin
    //
    joData    := _Json(AData);

    if joData.e = 'onchange' then begin
        //保存事件
        TDBComboBox(ACtrl).OnExit    := TDBComboBox(ACtrl).OnChange;
        //清空事件,以防止自动执行
        TDBComboBox(ACtrl).OnChange  := nil;
        //更新值
        sValue    := dwUnescape(joData.v);
        if TDBComboBox(ACtrl).Style = csDropDown then begin
            TDBComboBox(ACtrl).Text    := sValue;
        end else begin
            for I := 0 to TDBComboBox(ACtrl).Items.Count-1 do begin
                if sValue = TDBComboBox(ACtrl).Items[I] then begin
                    TDBComboBox(ACtrl).ItemIndex  := I;
                    break;
                end;
            end;
        end;
        //恢复事件
        TDBComboBox(ACtrl).OnChange  := TDBComboBox(ACtrl).OnExit;

        //执行事件
        if Assigned(TDBComboBox(ACtrl).OnChange) then begin
             TDBComboBox(ACtrl).OnChange(TDBComboBox(ACtrl));
        end;

        //清空OnExit事件
        TDBComboBox(ACtrl).OnExit  := nil;
    end else if joData.e = 'onblur' then begin
        //更新值
        sValue    := dwUnescape(joData.v);
        if sValue <> '' then begin
            //保证空字符的传递
            if sValue = '[!empty!]' then begin
                sValue  := '';
            end;


            //保存事件
            TDBComboBox(ACtrl).OnExit    := TDBComboBox(ACtrl).OnChange;
            //清空事件,以防止自动执行
            TDBComboBox(ACtrl).OnChange  := nil;
            if TDBComboBox(ACtrl).Style = csDropDown then begin
                TDBComboBox(ACtrl).Text    := sValue;
            end else begin
                for I := 0 to TDBComboBox(ACtrl).Items.Count-1 do begin
                    if sValue = TDBComboBox(ACtrl).Items[I] then begin
                        TDBComboBox(ACtrl).ItemIndex  := I;
                        break;
                    end;
                end;
            end;
            //恢复事件
            TDBComboBox(ACtrl).OnChange  := TDBComboBox(ACtrl).OnExit;

            //执行事件
            if Assigned(TDBComboBox(ACtrl).OnChange) then begin
                 TDBComboBox(ACtrl).OnChange(TDBComboBox(ACtrl));
            end;

            //清空OnExit事件
            TDBComboBox(ACtrl).OnExit  := nil;
        end;
    end else if joData.e = 'ondropdown' then begin
        if joData.v = 'true' then begin
             if Assigned(TDBComboBox(ACtrl).OnDropDown) then begin
                  TDBComboBox(ACtrl).OnDropDown(TDBComboBox(ACtrl));
             end;
        end;
    end;
end;


//取得HTML头部消息
function dwGetHead(ACtrl:TComponent):string;StdCall;
var
    sCode   : string;
    sBlur   : String;
    sName   : String;
    joHint  : Variant;
    joRes   : Variant;
begin
    //生成返回值数组
    joRes    := _Json('[]');

    //取得HINT对象JSON
    joHint    := dwGetHintJson(TControl(ACtrl));

    with TDBComboBox(ACtrl) do begin
        //得到名称备用
        sName   := dwFullName(Actrl);

        //生成blur时的代码， 主要用于解决可输入可选择功能
        //sBlur   := ' @blur=function(e){'+sName+'__txt=e.target.value;$forceUpdate();'
        //        +'dwevent(null,'''+sName+''',''this.'+sName+'__txt'',''onchange'','''+IntToStr(TForm(Owner).Handle)+''')}';
        sBlur   := ' @blur=function(e){'
                        +sName+'__txt=e.target.value;'
                        +'$forceUpdate();'
                        +'if(e.target.value==''''){'
                            +'dwevent(null,'''+sName+''',''"[!empty!]"'',''onblur'','''+IntToStr(TForm(Owner).Handle)+''');'
                        +'}else{'
                            +'dwevent(null,'''+sName+''',''this.'+sName+'__txt'',''onblur'','''+IntToStr(TForm(Owner).Handle)+''');'
                        +'}'
                +'}';

        //
        joRes.Add('<el-select'
                +' id="'+sName+'"'
                +' v-model="'+sName+'__txt"'
                +dwIIF(Style=csDropDown,' filterable','')
                +dwVisible(TControl(ACtrl))
                +dwDisable(TControl(ACtrl))
                +dwGetDWAttr(joHint)
                +' :style="{'
                    +'backgroundColor:'+sName+'__col,'
                    +'left:'+sName+'__lef,'
                    +'top:'+sName+'__top,'
                    +'width:'+sName+'__wid,'
                    +'height:'+sName+'__hei'
                +'}"'
                +' style="position:absolute;'
                    +'border:1px solid #DCDFF0;'
                    +'border-radius:3px;'
                    +dwGetHintStyle(joHint,'radius','border-radius','')   //border-radius
                    +dwGetHintStyle(joHint,'backgroundcolor','background-color','')       //自定义背景色
                    +dwGetHintStyle(joHint,'color','color','')             //自定义字体色
                    +dwGetHintStyle(joHint,'fontsize','font-size','')      //自定义字体大小
                    +dwGetDWStyle(joHint)
                +'"' //style 封闭
                //+dwIIF(Style=csDropDown,sBlur,'')
                +sBlur
                //+dwIIF(Assigned(OnDropDown) OR Assigned(OnCloseUp),'@visible-change="dwevent($event,'''+sName+''',$event,''ondropdown'','''')"','')
                +Format(_DWEVENT,['change',Name,'this.'+sName+'__txt','onchange',TForm(Owner).Handle])
                +'>');
        joRes.Add('    <el-option v-for="item in '+sName+'__its" :key="item.value" :label="item.value" :value="item.value"/>');

    end;

    //
    Result    := (joRes);
end;

//取得HTML尾部消息
function dwGetTail(ACtrl:TComponent):string;StdCall;
var
     joRes     : Variant;
begin
     //生成返回值数组
     joRes    := _Json('[]');
     //生成返回值数组
     joRes.Add('</el-select>');
     //
     Result    := (joRes);
end;

//取得Data
function dwGetData(ACtrl:TComponent):string;StdCall;
var
     joRes     : Variant;
     sCode     : string;
     iItem     : Integer;
begin
    //生成返回值数组
    joRes    := _Json('[]');
    //
    with TDBComboBox(ACtrl) do begin
        //添加选项
        sCode     := dwFullName(Actrl)+'__its:[';
        for iItem := 0 to Items.Count-1 do begin
             sCode     := sCode + '{value:'''+Items[iItem]+'''},';
        end;
        if Items.Count>0 then begin
             Delete(sCode,Length(sCode),1);     //删除最后的逗号
        end;
        sCode     := sCode + '],';
        joRes.Add(sCode);

        //
        joRes.Add(dwFullName(Actrl)+'__lef:"'+IntToStr(Left)+'px",');
        joRes.Add(dwFullName(Actrl)+'__top:"'+IntToStr(Top)+'px",');
        joRes.Add(dwFullName(Actrl)+'__wid:"'+IntToStr(Width)+'px",');
        if dwGetProp(TControl(ACtrl),'height')='' then begin
             joRes.Add(dwFullName(Actrl)+'__hei:"'+IntToStr(Height)+'px",');
        end else begin
             joRes.Add(dwFullName(Actrl)+'__hei:"'+dwGetProp(TControl(ACtrl),'height')+'px",');
        end;
        //
        joRes.Add(dwFullName(Actrl)+'__vis:'+dwIIF(Visible,'true,','false,'));
        joRes.Add(dwFullName(Actrl)+'__dis:'+dwIIF(Enabled,'false,','true,'));
        //
        joRes.Add(dwFullName(Actrl)+'__txt:"'+Text+'",');
        //
        if Color = clNone then begin
            joRes.Add(dwFullName(Actrl)+'__col:"rgba(0,0,0,0)",');
        end else begin
            joRes.Add(dwFullName(Actrl)+'__col:"'+dwAlphaColor(TPanel(ACtrl))+'",');
        end;
    end;
    //
    Result    := (joRes);
end;

//取得Method
function dwGetAction(ACtrl:TComponent):string;StdCall;
var
     joRes     : Variant;
     sCode     : string;
     iItem     : Integer;
begin
    //生成返回值数组
    joRes    := _Json('[]');
    //
    with TDBComboBox(ACtrl) do begin
        //添加选项
        sCode     := 'this.'+dwFullName(Actrl)+'__its=[';
        for iItem := 0 to Items.Count-1 do begin
             sCode     := sCode + '{value:'''+Items[iItem]+'''},';
        end;
        if Items.Count>0 then begin
             Delete(sCode,Length(sCode),1);
        end;
        sCode     := sCode + '];';
        joRes.Add(sCode);
        //
        joRes.Add('this.'+dwFullName(Actrl)+'__lef="'+IntToStr(Left)+'px";');
        joRes.Add('this.'+dwFullName(Actrl)+'__top="'+IntToStr(Top)+'px";');
        joRes.Add('this.'+dwFullName(Actrl)+'__wid="'+IntToStr(Width)+'px";');
        if dwGetProp(TControl(ACtrl),'height')='' then begin
             joRes.Add('this.'+dwFullName(Actrl)+'__hei="'+IntToStr(Height)+'px";');
        end else begin
             joRes.Add('this.'+dwFullName(Actrl)+'__hei="'+dwGetProp(TControl(ACtrl),'height')+'px";');
        end;
        //
        joRes.Add('this.'+dwFullName(Actrl)+'__vis='+dwIIF(Visible,'true;','false;'));
        joRes.Add('this.'+dwFullName(Actrl)+'__dis='+dwIIF(Enabled,'false;','true;'));
        //
        joRes.Add('this.'+dwFullName(Actrl)+'__txt="'+Text+'";');
        //
        if Color = clNone then begin
            joRes.Add('this.'+dwFullName(Actrl)+'__col="rgba(0,0,0,0)";');
        end else begin
            joRes.Add('this.'+dwFullName(Actrl)+'__col="'+dwAlphaColor(TPanel(ACtrl))+'";');
        end;
    end;
    //
    Result    := (joRes);
end;


exports
     //dwGetExtra,
     dwGetEvent,
     dwGetHead,
     dwGetTail,
     dwGetAction,
     dwGetData;
     
begin
end.
 
