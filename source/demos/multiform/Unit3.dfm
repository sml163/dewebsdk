object Form3: TForm3
  Left = 0
  Top = 0
  HelpType = htKeyword
  HelpKeyword = 'embed'
  Align = alClient
  BorderStyle = bsNone
  Caption = 'Form3'
  ClientHeight = 678
  ClientWidth = 1047
  Color = clAppWorkSpace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 20
    Top = 20
    Width = 1007
    Height = 638
    Margins.Left = 20
    Margins.Top = 20
    Margins.Right = 20
    Margins.Bottom = 20
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 0
    object Label1: TLabel
      Left = 1
      Top = 588
      Width = 1005
      Height = 96
      Align = alTop
      Alignment = taCenter
      AutoSize = False
      Caption = 'This is Form3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -27
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Layout = tlCenter
      ExplicitTop = 1
      ExplicitWidth = 446
    end
    object DBGrid1: TDBGrid
      AlignWithMargins = True
      Left = 1
      Top = 1
      Width = 1004
      Height = 587
      Hint = 
        '{"merge":[[3,5,8,"'#22522#26412#20449#24687'"],[2,7,8,"'#36890#20449#22320#22336'"]],"rowheight":40,"summary' +
        '":["'#27719#24635'",[6,"sum","'#21512#35745#65306'%.0f"],[6,"avg","'#24179#22343#65306'%.0f"],[9,"max","'#26368#22823#65306'%.0' +
        'f%%"],[9,"min","'#26368#23567#65306'%.0f%%"]]}'
      HelpType = htKeyword
      HelpKeyword = 'ww'
      Margins.Left = 0
      Margins.Top = 0
      Margins.Right = 1
      Margins.Bottom = 0
      Align = alTop
      Ctl3D = True
      DataSource = DataSource1
      FixedColor = 16316664
      Font.Charset = ANSI_CHARSET
      Font.Color = 9868950
      Font.Height = -15
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      Options = [dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -17
      TitleFont.Name = #24494#36719#38597#40657
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          Title.Caption = '{"type": "check"}'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          Title.Caption = '{"type": "index","caption":"'#24207#21495'"}'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          Title.Caption = 
            '{"type":"button","caption":"'#25805#20316'","list":[["'#23457#26680'","primary"],["'#26597#30475'","' +
            'success"],["'#21024#38500'","danger"]]}'
          Width = 200
          Visible = True
        end>
    end
    object Edit1: TEdit
      Left = 40
      Top = 96
      Width = 121
      Height = 21
      TabOrder = 1
      Text = 'Edit1'
      OnKeyPress = Edit1KeyPress
    end
    object Edit2: TEdit
      Left = 40
      Top = 128
      Width = 121
      Height = 21
      TabOrder = 2
      Text = 'Edit2'
      OnKeyPress = Edit2KeyPress
    end
    object Edit3: TEdit
      Left = 40
      Top = 160
      Width = 121
      Height = 21
      TabOrder = 3
      Text = 'Edit3'
      OnKeyPress = Edit3KeyPress
    end
  end
  object ADOQuery1: TADOQuery
    Parameters = <>
    Left = 164
    Top = 348
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery2
    Left = 72
    Top = 448
  end
  object ADOQuery2: TADOQuery
    Parameters = <>
    Left = 168
    Top = 448
  end
end
