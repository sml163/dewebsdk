﻿unit Unit_Inventory;

interface

uses
    //
    Unit_Show,

    //
    dwBase,
    dwAccess,
    dwSGUnit,

    //
    SynCommons{用于解析JSON},

    //
    Math,
    Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
    Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.Grids,
    Data.DB, Data.Win.ADODB, Vcl.DBGrids;

type
  TForm_Inventory = class(TForm)
    Panel1: TPanel;
    Edit_Search: TEdit;
    Button_Search2: TButton;
    TrackBar1: TTrackBar;
    ADOQuery1: TADOQuery;
    Button_Show: TButton;
    DBGrid1: TDBGrid;
    DataSource1: TDataSource;
    procedure Button_Search2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure StringGrid1GetEditMask(Sender: TObject; ACol, ARow: Integer; var Value: string);
    procedure Edit_SearchKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Button_ShowClick(Sender: TObject);
    procedure DBGrid1EndDock(Sender, Target: TObject; X, Y: Integer);
  private
    gsOrder : string;
    gjoFilter : variant;
    { Private declarations }
  public
    { Public declarations }
  end;


implementation

uses
    Unit1;

const
    _Fields : array[0..7] of String = ('ID','名称','型号','供应商','仓库','单位','单价','数量');

function dwGetFilter(
        AFilter:Variant
        ):string;
var
    iFilter : Integer;
    iCol    : Integer;
    iKey    : Integer;
    joCol   : Variant;
begin
    //::根据json格式的对象，得到过滤的SQL语句（部分）
    //AFilter = [{"filter":["电视机","空调"],"col":1}]
    //col出错，为8

    //
    Result  := '';
    if AFilter._Count > 0 then begin
        for iFilter := 0 to AFilter._Count-1 do begin
            joCol   := AFilter._(iFilter);
            //showMessage(IntToStr(joCol.col));
            if (joCol.filter._Count>0) and (joCol.col<Length(_Fields)) then begin
                Result  := Result +' AND (';
                iCol    := joCol.col;
                for iKey := 0 to joCol.filter._Count-1 do begin
                    Result  := Result + _Fields[iCol] +' like ''%'+joCol.filter._(iKey)+'%'' OR '
                end;
                Delete(Result,Length(Result)-3,4);
            end;
            Result  := Result +')';
        end;
    end;

end;


{$R *.dfm}

procedure TForm_Inventory.Button_Search2Click(Sender: TObject);
var
    iRecCount   : Integer;
begin
    DataSource1.DataSet := nil;
    dwaGetPageData(ADOQuery1,'wms_Inventory','*',
        dwaGetWhere(ADOQuery1,'wms_Inventory',Edit_Search.Text),//'WHERE (1=1)',
        gsOrder,
        1,
        10,
        iRecCount);
    TrackBar1.Max   := iRecCount;
    DataSource1.DataSet := ADOQuery1;

end;

procedure TForm_Inventory.Button_ShowClick(Sender: TObject);
var
    oForm   : TForm1;
begin
    oForm   := TForm1(self.Owner);  //注意：这里不能直接使用Form1变量！！！，需要相对self取得
    if oForm.Form_Show = nil then begin
        oForm.Form_Show  := TForm_Show.Create(oForm);
        oForm.Form_Show.Parent   := oForm; //设置新窗体的Parent
        //
        DockSite    := True;
    end;
    //
    dwShowModalPro(self,oForm.Form_Show);

end;

procedure TForm_Inventory.DBGrid1EndDock(Sender, Target: TObject; X, Y: Integer);
var
    iRecCount   : Integer;
begin
    case X of
        1 : begin   //升序
            gsOrder := ' ORDER BY 单价,ID ';
        end;
        2 : begin   //逆序
            gsOrder := ' ORDER BY 单价 DESC ,ID DESC';
        end;
    end;
    //
    DataSource1.DataSet := nil;
    dwaGetPageData(ADOQuery1,'wms_Inventory','*',
        dwaGetWhere(ADOQuery1,'wms_Inventory',Edit_Search.Text),//'WHERE (1=1)',
        gsOrder,
        1,
        10,
        iRecCount);
    TrackBar1.Max   := iRecCount;
    DataSource1.DataSet := ADOQuery1;
end;

procedure TForm_Inventory.Edit_SearchKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    Button_Search2.OnClick(Button_Search2);
end;

procedure TForm_Inventory.FormCreate(Sender: TObject);
begin
    //
    gsOrder     := ' ORDER BY id';

    //
    gjoFilter   := _json('[]');
end;

procedure TForm_Inventory.StringGrid1GetEditMask(Sender: TObject; ACol, ARow: Integer;  var Value: string);
var
    I       : Integer;
    bFound  : Boolean;
    joFilter: Variant;
var
    joValue : Variant;
begin
(*
    //数据合法性检验，主要检查是否JSON格式
    if dwStrIsJson(Value) then begin
        //将字符串转化为JSON对象
        joValue := _json(Value);

        //根据事件来源进行处理
        if joValue.type = 'sort' then begin
            //返回一个JSON字符串到 OnGetEditMask 的参数中
            //形如：{"type":"sort","col":3,"order":"0"}
            //其中，type为类型，主要与选择、过滤等分开，
            //col 为列序号，从0开始
            //order 为顺序，1为升序，0为降序

            //::处理客户端的排序操作消息
            ACol    := joValue.col;

            //根据排序列得到排序字符串
            if joValue.order = 1 then begin  //ARow = 1 表示升序，0 表示降序
                if joValue.col > 0 then begin  //排序字段，第0个为ID， 如果不是第0个，则增加一个备用排序，以解决排序字段多个相同的分页问题
                    gsOrder := ' ORDER BY '+_Fields[ACol]+',ID ';
                end else begin
                    gsOrder := ' ORDER BY '+_Fields[ACol]+' ';
                end;
            end else begin          //ARow = 1 表示升序，0 表示降序
                if joValue.col > 0 then begin  //排序字段，第0个为ID， 如果不是第0个，则增加一个备用排序，以解决排序字段多个相同的分页问题
                    gsOrder := ' ORDER BY '+_Fields[ACol]+' DESC,ID DESC ';
                end else begin
                    gsOrder := ' ORDER BY '+_Fields[ACol]+' DESC ';
                end;
            end;

            //更新数据
            dwaGetDataToGrid(ADOQuery1,'wms_Inventory','ID,名称,型号,供应商,仓库,单位,单价,数量',
                    dwaGetWhere(ADOQuery1,'wms_Inventory',Edit_Search.Text)+dwGetFilter(gjoFilter),
                    gsOrder,1,10,StringGrid1,TrackBar1);
        end else if joValue.type = 'filter' then begin
            if joValue.data._Count > 0 then begin
                //根据当前筛选字符串生成JSON对象
                joFilter        := _json('{"filter":'+VariantSaveJSON(joValue.data)+'}');
                joFilter.col    := joValue.col;    //保存列消息

                //在目前的全部筛选JSON对象中查找是否有当前列的. 如果有,则更新当前列筛选信息;如果没有,则添加
                bFound  := False;
                for I := 0 to gjoFilter._Count-1 do begin
                    if gjoFilter._(i).col = ACol then begin
                        bFound  := True;
                        gjoFilter._(i).filter   := joFilter.filter;
                        break;
                    end;
                end;

                //如果目前的全部筛选JSON对象中没有当前列的筛选信息,则添加
                if not bFound then begin
                    gjoFilter.Add(joFilter);
                end;

            end else begin
                //表示为重置当前列的筛选
                for I := 0 to gjoFilter._Count-1 do begin
                    if gjoFilter._(i).col = ACol then begin
                        gjoFilter.Delete(i);
                        break;
                    end;
                end;
            end;

            //根据当前全部筛选JSON对象的信息生成WHERE字符串
            dwaGetDataToGrid(ADOQuery1,'wms_Inventory','ID,名称,型号,供应商,仓库,单位,单价,数量',
                dwaGetWhere(ADOQuery1,'wms_Inventory',Edit_Search.Text)+dwGetFilter(gjoFilter),
                gsOrder,
                1,
                10,
                StringGrid1,TrackBar1);
        end;
    end;

*)
end;

procedure TForm_Inventory.TrackBar1Change(Sender: TObject);
var
    iRecCount   : Integer;
begin
    DataSource1.DataSet := nil;
    dwaGetPageData(ADOQuery1,'wms_Inventory','*',
        dwaGetWhere(ADOQuery1,'wms_Inventory',Edit_Search.Text),//'WHERE (1=1)',
        gsOrder,
        TrackBar1.Position,
        10,
        iRecCount);
    TrackBar1.Max   := iRecCount;
    DataSource1.DataSet := ADOQuery1;
end;

end.
