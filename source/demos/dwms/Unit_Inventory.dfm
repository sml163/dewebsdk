object Form_Inventory: TForm_Inventory
  Left = 0
  Top = 0
  HelpKeyword = 'embed'
  HelpContext = 3344
  Caption = 'Form_Inventory'
  ClientHeight = 619
  ClientWidth = 883
  Color = clWhite
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 20
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 883
    Height = 53
    Hint = '{"dwstyle":"border-bottom:solid 1px #ed6d00;"}'
    Align = alTop
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 0
    object Edit_Search: TEdit
      AlignWithMargins = True
      Left = 10
      Top = 11
      Width = 503
      Height = 30
      Hint = 
        '{"placeholder":"'#35831#36755#20837#26597#35810#20851#38190#23383','#25903#25345#22810#20851#38190#23383#25628#32034'","radius":"15px","suffix-icon"' +
        ':"el-icon-search","dwstyle":"padding-left:10px;"}'
      Margins.Left = 10
      Margins.Top = 11
      Margins.Right = 1
      Margins.Bottom = 12
      Align = alLeft
      TabOrder = 0
      OnKeyUp = Edit_SearchKeyUp
      ExplicitHeight = 28
    end
    object Button_Search2: TButton
      AlignWithMargins = True
      Left = 515
      Top = 11
      Width = 78
      Height = 32
      Hint = 
        '{"type":"primary","icon":"el-icon-search","radius":"0 3px 3px 0"' +
        '}'
      Margins.Left = 1
      Margins.Top = 11
      Margins.Bottom = 10
      Align = alLeft
      Caption = #26597#35810
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -15
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Visible = False
      OnClick = Button_Search2Click
    end
    object Button_Show: TButton
      AlignWithMargins = True
      Left = 597
      Top = 11
      Width = 163
      Height = 32
      Hint = '{"type":"success"}'
      Margins.Left = 1
      Margins.Top = 11
      Margins.Bottom = 10
      Align = alLeft
      Caption = #24377#20986#24335#31383#20307
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -15
      Font.Name = #24494#36719#38597#40657
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = Button_ShowClick
    end
  end
  object TrackBar1: TTrackBar
    AlignWithMargins = True
    Left = 10
    Top = 460
    Width = 863
    Height = 30
    HelpType = htKeyword
    HelpKeyword = 'wwpage'
    Margins.Left = 10
    Margins.Top = 10
    Margins.Right = 10
    Margins.Bottom = 20
    Align = alTop
    PageSize = 10
    TabOrder = 1
    OnChange = TrackBar1Change
  end
  object DBGrid1: TDBGrid
    AlignWithMargins = True
    Left = 10
    Top = 63
    Width = 863
    Height = 387
    Hint = '{"dwattr":"stripe"}'
    HelpType = htKeyword
    HelpKeyword = 'ww'
    HelpContext = 40
    Margins.Left = 10
    Margins.Top = 10
    Margins.Right = 10
    Margins.Bottom = 0
    Align = alTop
    DataSource = DataSource1
    Font.Charset = ANSI_CHARSET
    Font.Color = 9868950
    Font.Height = -16
    Font.Name = #24494#36719#38597#40657
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -16
    TitleFont.Name = #24494#36719#38597#40657
    TitleFont.Style = []
    OnEndDock = DBGrid1EndDock
    Columns = <
      item
        Expanded = False
        FieldName = #21517#31216
        Width = 130
        Visible = True
      end
      item
        Expanded = False
        FieldName = #22411#21495
        Width = 130
        Visible = True
      end
      item
        Expanded = False
        FieldName = #20179#24211
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = #21333#20301
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = #21333#20215
        Title.Caption = '{"sort":1}'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = #25968#37327
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = #20379#24212#21830
        Width = 200
        Visible = True
      end>
  end
  object ADOQuery1: TADOQuery
    Parameters = <>
    Left = 144
    Top = 376
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 144
    Top = 440
  end
end
