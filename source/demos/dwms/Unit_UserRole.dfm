object Form_UserRole: TForm_UserRole
  Left = 0
  Top = 0
  HelpType = htKeyword
  HelpKeyword = 'embed'
  Caption = 'Form_UserRole'
  ClientHeight = 657
  ClientWidth = 931
  Color = clWhite
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = #24494#36719#38597#40657
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnStartDock = FormStartDock
  PixelsPerInch = 96
  TextHeight = 20
  object Panel_L: TPanel
    Left = 0
    Top = 0
    Width = 200
    Height = 657
    Align = alLeft
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 0
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 200
      Height = 30
      Hint = '{"dwstyle":"border-bottom:solid 1px #ddd"}'
      Align = alTop
      BevelKind = bkFlat
      BevelOuter = bvNone
      Color = clWhite
      ParentBackground = False
      TabOrder = 0
      object Label1: TLabel
        AlignWithMargins = True
        Left = 10
        Top = 3
        Width = 176
        Height = 20
        Margins.Left = 10
        Margins.Right = 10
        Align = alClient
        Alignment = taCenter
        AutoSize = False
        Caption = #35282#33394
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = #24494#36719#38597#40657
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
        ExplicitLeft = 72
        ExplicitTop = 16
        ExplicitWidth = 30
      end
    end
    object Panel_Roles: TPanel
      Left = 0
      Top = 30
      Width = 200
      Height = 627
      Align = alClient
      Color = clWhite
      ParentBackground = False
      TabOrder = 1
      object Button1: TButton
        AlignWithMargins = True
        Left = 4
        Top = 4
        Width = 192
        Height = 29
        Hint = '{"type":"primary"}'
        Align = alTop
        Caption = #31649#29702#21592
        Font.Charset = ANSI_CHARSET
        Font.Color = clGray
        Font.Height = -15
        Font.Name = #24494#36719#38597#40657
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnClick = Button1Click
      end
      object Button2: TButton
        AlignWithMargins = True
        Left = 4
        Top = 39
        Width = 192
        Height = 29
        Align = alTop
        Caption = 'Button1'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGray
        Font.Height = -15
        Font.Name = #24494#36719#38597#40657
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        OnClick = Button1Click
      end
      object Button3: TButton
        AlignWithMargins = True
        Left = 4
        Top = 74
        Width = 192
        Height = 29
        Align = alTop
        Caption = 'Button1'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGray
        Font.Height = -15
        Font.Name = #24494#36719#38597#40657
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnClick = Button1Click
      end
      object Button4: TButton
        AlignWithMargins = True
        Left = 4
        Top = 109
        Width = 192
        Height = 29
        Align = alTop
        Caption = 'Button1'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGray
        Font.Height = -15
        Font.Name = #24494#36719#38597#40657
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        OnClick = Button1Click
      end
      object Button5: TButton
        AlignWithMargins = True
        Left = 4
        Top = 144
        Width = 192
        Height = 29
        Align = alTop
        Caption = 'Button1'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGray
        Font.Height = -15
        Font.Name = #24494#36719#38597#40657
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        OnClick = Button1Click
      end
      object Button6: TButton
        AlignWithMargins = True
        Left = 4
        Top = 179
        Width = 192
        Height = 29
        Align = alTop
        Caption = 'Button1'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGray
        Font.Height = -15
        Font.Name = #24494#36719#38597#40657
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        OnClick = Button1Click
      end
      object Button7: TButton
        AlignWithMargins = True
        Left = 4
        Top = 214
        Width = 192
        Height = 29
        Align = alTop
        Caption = 'Button1'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGray
        Font.Height = -15
        Font.Name = #24494#36719#38597#40657
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        OnClick = Button1Click
      end
      object Button8: TButton
        AlignWithMargins = True
        Left = 4
        Top = 249
        Width = 192
        Height = 29
        Align = alTop
        Caption = #20986#24211#21592
        Font.Charset = ANSI_CHARSET
        Font.Color = clGray
        Font.Height = -15
        Font.Name = #24494#36719#38597#40657
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        OnClick = Button1Click
      end
      object Button9: TButton
        AlignWithMargins = True
        Left = 4
        Top = 284
        Width = 192
        Height = 29
        Align = alTop
        Caption = #20837#24211#21592
        Font.Charset = ANSI_CHARSET
        Font.Color = clGray
        Font.Height = -15
        Font.Name = #24494#36719#38597#40657
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        OnClick = Button1Click
      end
      object Button10: TButton
        AlignWithMargins = True
        Left = 4
        Top = 319
        Width = 192
        Height = 29
        Align = alTop
        Caption = #31649#29702#21592
        Font.Charset = ANSI_CHARSET
        Font.Color = clGray
        Font.Height = -15
        Font.Name = #24494#36719#38597#40657
        Font.Style = []
        ParentFont = False
        TabOrder = 9
        OnClick = Button1Click
      end
      object Panel5: TPanel
        Left = 1
        Top = 351
        Width = 198
        Height = 39
        Hint = '{"dwstyle":"border-top:solid 1px #ddd"}'
        Align = alTop
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 16448250
        ParentBackground = False
        TabOrder = 10
        object Button_Delete: TButton
          AlignWithMargins = True
          Left = 131
          Top = 3
          Width = 60
          Height = 29
          Hint = '{"type":"primary"}'
          Align = alRight
          Caption = #21024#38500
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -15
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = Button_DeleteClick
        end
        object Button_Add: TButton
          AlignWithMargins = True
          Left = 3
          Top = 3
          Width = 60
          Height = 29
          Hint = '{"type":"primary"}'
          Align = alLeft
          Caption = #22686#21152
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -15
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          OnClick = Button_AddClick
        end
        object Button_Rename: TButton
          AlignWithMargins = True
          Left = 69
          Top = 3
          Width = 56
          Height = 29
          Hint = '{"type":"primary"}'
          Align = alClient
          Caption = #37325#21629#21517
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -15
          Font.Name = #24494#36719#38597#40657
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          OnClick = Button_RenameClick
        end
      end
    end
  end
  object Panel_C: TPanel
    Left = 200
    Top = 0
    Width = 731
    Height = 657
    Align = alClient
    BevelOuter = bvNone
    Color = clWhite
    ParentBackground = False
    TabOrder = 1
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 731
      Height = 30
      Hint = '{"dwstyle":"border-bottom:solid 1px #ddd"}'
      Align = alTop
      BevelKind = bkFlat
      BevelOuter = bvNone
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = #24494#36719#38597#40657
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 0
      object Label2: TLabel
        AlignWithMargins = True
        Left = 10
        Top = 3
        Width = 707
        Height = 20
        Margins.Left = 10
        Margins.Right = 10
        Align = alClient
        AutoSize = False
        Caption = #35282' '#33394' '#26435' '#38480
        Layout = tlCenter
        ExplicitLeft = 80
        ExplicitTop = 16
        ExplicitWidth = 30
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 520
      Width = 731
      Height = 35
      Hint = '{"dwstyle":"border-bottom:solid 1px #ddd"}'
      Align = alTop
      Color = clWhite
      ParentBackground = False
      TabOrder = 1
      object Label_S15: TLabel
        AlignWithMargins = True
        Left = 11
        Top = 4
        Width = 300
        Height = 27
        Margins.Left = 10
        Align = alLeft
        AutoSize = False
        Caption = #31995#32479#37197#32622
        Layout = tlCenter
        ExplicitTop = -4
      end
      object ToggleSwitch_15: TToggleSwitch
        AlignWithMargins = True
        Left = 317
        Top = 6
        Width = 200
        Height = 25
        Margins.Top = 5
        Align = alLeft
        AutoSize = False
        StateCaptions.CaptionOn = ' '
        StateCaptions.CaptionOff = ' '
        SwitchWidth = 40
        TabOrder = 0
        ThumbColor = 16752192
        ThumbWidth = 10
        OnClick = ToggleSwitch_1Click
      end
    end
    object Panel7: TPanel
      Left = 0
      Top = 415
      Width = 731
      Height = 35
      Hint = '{"dwstyle":"border-bottom:solid 1px #ddd"}'
      Align = alTop
      Color = clWhite
      ParentBackground = False
      TabOrder = 2
      object Label_S12: TLabel
        AlignWithMargins = True
        Left = 11
        Top = 4
        Width = 300
        Height = 27
        Margins.Left = 10
        Align = alLeft
        AutoSize = False
        Caption = #20998#31867#32479#35745
        Layout = tlCenter
      end
      object ToggleSwitch_12: TToggleSwitch
        AlignWithMargins = True
        Left = 317
        Top = 6
        Width = 200
        Height = 25
        Margins.Top = 5
        Align = alLeft
        AutoSize = False
        StateCaptions.CaptionOn = ' '
        StateCaptions.CaptionOff = ' '
        SwitchWidth = 40
        TabOrder = 0
        ThumbColor = 16752192
        ThumbWidth = 10
        OnClick = ToggleSwitch_1Click
      end
    end
    object Panel8: TPanel
      Left = 0
      Top = 380
      Width = 731
      Height = 35
      Hint = '{"dwstyle":"border-bottom:solid 1px #ddd"}'
      Align = alTop
      Color = clWhite
      ParentBackground = False
      TabOrder = 3
      object Label_S11: TLabel
        AlignWithMargins = True
        Left = 11
        Top = 4
        Width = 300
        Height = 27
        Margins.Left = 10
        Align = alLeft
        AutoSize = False
        Caption = #26597#35810#20986#24211
        Layout = tlCenter
      end
      object ToggleSwitch_11: TToggleSwitch
        AlignWithMargins = True
        Left = 317
        Top = 6
        Width = 200
        Height = 25
        Margins.Top = 5
        Align = alLeft
        AutoSize = False
        StateCaptions.CaptionOn = ' '
        StateCaptions.CaptionOff = ' '
        SwitchWidth = 40
        TabOrder = 0
        ThumbColor = 16752192
        ThumbWidth = 10
        OnClick = ToggleSwitch_1Click
      end
    end
    object Panel9: TPanel
      Left = 0
      Top = 345
      Width = 731
      Height = 35
      Hint = '{"dwstyle":"border-bottom:solid 1px #ddd"}'
      Align = alTop
      Color = clWhite
      ParentBackground = False
      TabOrder = 4
      object Label_S10: TLabel
        AlignWithMargins = True
        Left = 11
        Top = 4
        Width = 300
        Height = 27
        Margins.Left = 10
        Align = alLeft
        AutoSize = False
        Caption = #26597#35810#20837#24211
        Layout = tlCenter
      end
      object ToggleSwitch_10: TToggleSwitch
        AlignWithMargins = True
        Left = 317
        Top = 6
        Width = 200
        Height = 25
        Margins.Top = 5
        Align = alLeft
        AutoSize = False
        StateCaptions.CaptionOn = ' '
        StateCaptions.CaptionOff = ' '
        SwitchWidth = 40
        TabOrder = 0
        ThumbColor = 16752192
        ThumbWidth = 10
        OnClick = ToggleSwitch_1Click
      end
    end
    object Panel10: TPanel
      Left = 0
      Top = 310
      Width = 731
      Height = 35
      Hint = '{"dwstyle":"border-bottom:solid 1px #ddd"}'
      Align = alTop
      Color = clWhite
      ParentBackground = False
      TabOrder = 5
      object Label_S9: TLabel
        AlignWithMargins = True
        Left = 11
        Top = 4
        Width = 300
        Height = 27
        Margins.Left = 10
        Align = alLeft
        AutoSize = False
        Caption = #39046#26009#21333#20301#20449#24687#31649#29702
        Layout = tlCenter
        ExplicitLeft = 9
      end
      object ToggleSwitch_9: TToggleSwitch
        AlignWithMargins = True
        Left = 317
        Top = 6
        Width = 200
        Height = 25
        Margins.Top = 5
        Align = alLeft
        AutoSize = False
        StateCaptions.CaptionOn = ' '
        StateCaptions.CaptionOff = ' '
        SwitchWidth = 40
        TabOrder = 0
        ThumbColor = 16752192
        ThumbWidth = 10
        OnClick = ToggleSwitch_1Click
      end
    end
    object Panel11: TPanel
      Left = 0
      Top = 275
      Width = 731
      Height = 35
      Hint = '{"dwstyle":"border-bottom:solid 1px #ddd"}'
      Align = alTop
      Color = clWhite
      ParentBackground = False
      TabOrder = 6
      object Label_S8: TLabel
        AlignWithMargins = True
        Left = 11
        Top = 4
        Width = 300
        Height = 27
        Margins.Left = 10
        Align = alLeft
        AutoSize = False
        Caption = #20379#24212#21830#20449#24687#31649#29702
        Layout = tlCenter
      end
      object ToggleSwitch_8: TToggleSwitch
        AlignWithMargins = True
        Left = 317
        Top = 6
        Width = 200
        Height = 25
        Margins.Top = 5
        Align = alLeft
        AutoSize = False
        StateCaptions.CaptionOn = ' '
        StateCaptions.CaptionOff = ' '
        SwitchWidth = 40
        TabOrder = 0
        ThumbColor = 16752192
        ThumbWidth = 10
        OnClick = ToggleSwitch_1Click
      end
    end
    object Panel12: TPanel
      Left = 0
      Top = 240
      Width = 731
      Height = 35
      Hint = '{"dwstyle":"border-bottom:solid 1px #ddd"}'
      Align = alTop
      Color = clWhite
      ParentBackground = False
      TabOrder = 7
      object Label_S7: TLabel
        AlignWithMargins = True
        Left = 11
        Top = 4
        Width = 300
        Height = 27
        Margins.Left = 10
        Align = alLeft
        AutoSize = False
        Caption = #20179#24211#20449#24687#31649#29702
        Layout = tlCenter
      end
      object ToggleSwitch_7: TToggleSwitch
        AlignWithMargins = True
        Left = 317
        Top = 6
        Width = 200
        Height = 25
        Margins.Top = 5
        Align = alLeft
        AutoSize = False
        StateCaptions.CaptionOn = ' '
        StateCaptions.CaptionOff = ' '
        SwitchWidth = 40
        TabOrder = 0
        ThumbColor = 16752192
        ThumbWidth = 10
        OnClick = ToggleSwitch_1Click
      end
    end
    object Panel13: TPanel
      Left = 0
      Top = 205
      Width = 731
      Height = 35
      Hint = '{"dwstyle":"border-bottom:solid 1px #ddd"}'
      Align = alTop
      Color = clWhite
      ParentBackground = False
      TabOrder = 8
      object Label_S6: TLabel
        AlignWithMargins = True
        Left = 11
        Top = 4
        Width = 300
        Height = 27
        Margins.Left = 10
        Align = alLeft
        AutoSize = False
        Caption = #20135#21697#20449#24687#31649#29702
        Layout = tlCenter
      end
      object ToggleSwitch_6: TToggleSwitch
        AlignWithMargins = True
        Left = 317
        Top = 6
        Width = 200
        Height = 25
        Margins.Top = 5
        Align = alLeft
        AutoSize = False
        StateCaptions.CaptionOn = ' '
        StateCaptions.CaptionOff = ' '
        SwitchWidth = 40
        TabOrder = 0
        ThumbColor = 16752192
        ThumbWidth = 10
        OnClick = ToggleSwitch_1Click
      end
    end
    object Panel14: TPanel
      Left = 0
      Top = 170
      Width = 731
      Height = 35
      Hint = '{"dwstyle":"border-bottom:solid 1px #ddd"}'
      Align = alTop
      Color = clWhite
      ParentBackground = False
      TabOrder = 9
      object Label_S5: TLabel
        AlignWithMargins = True
        Left = 11
        Top = 4
        Width = 300
        Height = 27
        Margins.Left = 10
        Align = alLeft
        AutoSize = False
        Caption = #35282#33394#26435#38480#31649#29702
        Layout = tlCenter
      end
      object ToggleSwitch_5: TToggleSwitch
        AlignWithMargins = True
        Left = 317
        Top = 6
        Width = 200
        Height = 25
        Margins.Top = 5
        Align = alLeft
        AutoSize = False
        StateCaptions.CaptionOn = ' '
        StateCaptions.CaptionOff = ' '
        SwitchWidth = 40
        TabOrder = 0
        ThumbColor = 16752192
        ThumbWidth = 10
        OnClick = ToggleSwitch_1Click
      end
    end
    object Panel15: TPanel
      Left = 0
      Top = 135
      Width = 731
      Height = 35
      Hint = '{"dwstyle":"border-bottom:solid 1px #ddd"}'
      Align = alTop
      Color = clWhite
      ParentBackground = False
      TabOrder = 10
      object Label_S4: TLabel
        AlignWithMargins = True
        Left = 11
        Top = 4
        Width = 300
        Height = 27
        Margins.Left = 10
        Align = alLeft
        AutoSize = False
        Caption = #29992#25143#20449#24687#31649#29702
        Layout = tlCenter
      end
      object ToggleSwitch_4: TToggleSwitch
        AlignWithMargins = True
        Left = 317
        Top = 6
        Width = 200
        Height = 25
        Margins.Top = 5
        Align = alLeft
        AutoSize = False
        StateCaptions.CaptionOn = ' '
        StateCaptions.CaptionOff = ' '
        SwitchWidth = 40
        TabOrder = 0
        ThumbColor = 16752192
        ThumbWidth = 10
        OnClick = ToggleSwitch_1Click
      end
    end
    object Panel16: TPanel
      Left = 0
      Top = 100
      Width = 731
      Height = 35
      Hint = '{"dwstyle":"border-bottom:solid 1px #ddd"}'
      Align = alTop
      Color = clWhite
      ParentBackground = False
      TabOrder = 11
      object Label_S3: TLabel
        AlignWithMargins = True
        Left = 11
        Top = 4
        Width = 300
        Height = 27
        Margins.Left = 10
        Align = alLeft
        AutoSize = False
        Caption = #20135#21697#20986#24211
        Layout = tlCenter
      end
      object ToggleSwitch_3: TToggleSwitch
        AlignWithMargins = True
        Left = 317
        Top = 6
        Width = 200
        Height = 25
        Margins.Top = 5
        Align = alLeft
        AutoSize = False
        StateCaptions.CaptionOn = ' '
        StateCaptions.CaptionOff = ' '
        SwitchWidth = 40
        TabOrder = 0
        ThumbColor = 16752192
        ThumbWidth = 10
        OnClick = ToggleSwitch_1Click
      end
    end
    object Panel17: TPanel
      Left = 0
      Top = 65
      Width = 731
      Height = 35
      Hint = '{"dwstyle":"border-bottom:solid 1px #ddd"}'
      Align = alTop
      Color = clWhite
      ParentBackground = False
      TabOrder = 12
      object Label_S2: TLabel
        AlignWithMargins = True
        Left = 11
        Top = 4
        Width = 300
        Height = 27
        Margins.Left = 10
        Align = alLeft
        AutoSize = False
        Caption = #20135#21697#20837#24211
        Layout = tlCenter
      end
      object ToggleSwitch_2: TToggleSwitch
        AlignWithMargins = True
        Left = 317
        Top = 6
        Width = 200
        Height = 25
        Margins.Top = 5
        Align = alLeft
        AutoSize = False
        StateCaptions.CaptionOn = ' '
        StateCaptions.CaptionOff = ' '
        SwitchWidth = 40
        TabOrder = 0
        ThumbColor = 16752192
        ThumbWidth = 10
        OnClick = ToggleSwitch_1Click
      end
    end
    object Panel18: TPanel
      Left = 0
      Top = 30
      Width = 731
      Height = 35
      Hint = '{"dwstyle":"border-bottom:solid 1px #ddd"}'
      Align = alTop
      Color = clWhite
      ParentBackground = False
      TabOrder = 13
      object Label_S1: TLabel
        AlignWithMargins = True
        Left = 11
        Top = 4
        Width = 300
        Height = 27
        Margins.Left = 10
        Align = alLeft
        AutoSize = False
        Caption = #24211#23384#26597#35810
        Layout = tlCenter
        ExplicitTop = 6
      end
      object ToggleSwitch_1: TToggleSwitch
        AlignWithMargins = True
        Left = 317
        Top = 6
        Width = 200
        Height = 25
        Margins.Top = 5
        Align = alLeft
        AutoSize = False
        StateCaptions.CaptionOn = ' '
        StateCaptions.CaptionOff = ' '
        SwitchWidth = 40
        TabOrder = 0
        ThumbColor = 16752192
        ThumbWidth = 10
        OnClick = ToggleSwitch_1Click
      end
    end
    object Panel1: TPanel
      Left = 0
      Top = 485
      Width = 731
      Height = 35
      Hint = '{"dwstyle":"border-bottom:solid 1px #ddd"}'
      Align = alTop
      Color = clWhite
      ParentBackground = False
      TabOrder = 14
      object Label_S14: TLabel
        AlignWithMargins = True
        Left = 11
        Top = 4
        Width = 300
        Height = 27
        Margins.Left = 10
        Align = alLeft
        AutoSize = False
        Caption = #31995#32479#35774#35745
        Layout = tlCenter
      end
      object ToggleSwitch_14: TToggleSwitch
        AlignWithMargins = True
        Left = 317
        Top = 6
        Width = 200
        Height = 25
        Margins.Top = 5
        Align = alLeft
        AutoSize = False
        StateCaptions.CaptionOn = ' '
        StateCaptions.CaptionOff = ' '
        SwitchWidth = 40
        TabOrder = 0
        ThumbColor = 16752192
        ThumbWidth = 10
        OnClick = ToggleSwitch_1Click
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 450
      Width = 731
      Height = 35
      Hint = '{"dwstyle":"border-bottom:solid 1px #ddd"}'
      Align = alTop
      Color = clWhite
      ParentBackground = False
      TabOrder = 15
      object Label_S13: TLabel
        AlignWithMargins = True
        Left = 11
        Top = 4
        Width = 300
        Height = 27
        Margins.Left = 10
        Align = alLeft
        AutoSize = False
        Caption = #36164#26009#31649#29702
        Layout = tlCenter
      end
      object ToggleSwitch_13: TToggleSwitch
        AlignWithMargins = True
        Left = 317
        Top = 6
        Width = 200
        Height = 25
        Margins.Top = 5
        Align = alLeft
        AutoSize = False
        StateCaptions.CaptionOn = ' '
        StateCaptions.CaptionOff = ' '
        SwitchWidth = 40
        TabOrder = 0
        ThumbColor = 16752192
        ThumbWidth = 10
        OnClick = ToggleSwitch_1Click
      end
    end
  end
  object ADOQuery1: TADOQuery
    Parameters = <>
    Left = 576
    Top = 176
  end
end
